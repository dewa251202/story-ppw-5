from django.urls import include, path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
	path('', include('profil.urls')),
	path('', include('story_1.urls')),
	path('', include('kuliah.urls'))
]
