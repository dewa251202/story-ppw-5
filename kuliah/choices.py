from datetime import date

PILIHAN_SEMESTER = [
	('Gasal', 'Gasal'),
	('Genap', 'Genap')
]

PILIHAN_TAHUN = []

THIS_YEAR = int(date.today().year)

for i in range(2000, THIS_YEAR + 1):
	u = str(i) + '/' + str(i + 1)
	v = u[:]
	PILIHAN_TAHUN.append((u, v))