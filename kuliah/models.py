from django.db import models
from .choices import *

class MataKuliah(models.Model):
	nama_matkul = models.CharField(max_length=50)
	nama_dosen = models.CharField(max_length=50)
	jumlah_sks = models.PositiveIntegerField()
	deskripsi = models.TextField(blank=True)
	semester = models.CharField(max_length=5, choices=PILIHAN_SEMESTER)
	tahun = models.CharField(max_length=9, choices=PILIHAN_TAHUN)
	ruang_kelas = models.CharField(max_length=50)
	