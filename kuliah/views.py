from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import MataKuliah
from .forms import InputMataKuliah
from .choices import *

def kuliah(request):
	matkul_db = MataKuliah.objects.all()
	context = {
		"input_matkul" : InputMataKuliah(initial={"tahun": PILIHAN_TAHUN[-1][0]}),
		"matkul_db" : matkul_db
	}
	
	return render(request, 'main/kuliah.html', context)

def tambah_matkul(request):
	detail_matkul = InputMataKuliah(request.POST or None)
	
	if(detail_matkul.is_valid and request.method == "POST"):
		detail_matkul.save()
	return HttpResponseRedirect('/kuliah')
	
def get_detail(request):
	id_matkul = request.GET['id_matkul']
	context = {
		"matkul" : MataKuliah.objects.get(id=id_matkul)
	}
	
	if(request.method == "GET"):
		return render(request, 'main/detail_matkul.html', context)
	else:
		return HttpResponseRedirect('/kuliah')
		
def hapus_matkul(request):
	query_dict = request.POST
	id_matkul = query_dict.get('id_matkul')
	
	if(request.method == "POST"):
		MataKuliah.objects.get(id=id_matkul).delete()
	return HttpResponseRedirect('/kuliah')