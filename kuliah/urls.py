from django.urls import include, path
from . import views

urlpatterns = [
	path('kuliah', views.kuliah, name='kuliah'),
	path('tambah_matkul', views.tambah_matkul, name='tambah_matkul'),
	path('hapus_matkul', views.hapus_matkul, name='hapus_matkul'),
	path('detail_matkul', views.get_detail, name='detail_matkul')
]