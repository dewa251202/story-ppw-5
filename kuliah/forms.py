from django import forms
from .models import MataKuliah
from .choices import *

class InputMataKuliah(forms.ModelForm):
	class Meta:
		model = MataKuliah
		fields = ['nama_matkul', 'nama_dosen', 'jumlah_sks', 'deskripsi', 'semester', 'tahun', 'ruang_kelas']
	
	nama_matkul = forms.CharField(label='Nama Mata Kuliah', max_length=50)
	nama_dosen = forms.CharField(label='Nama Dosen', max_length=50)
	jumlah_sks = forms.IntegerField(label='Jumlah SKS', min_value=1)
	deskripsi = forms.CharField(label='Deskripsi', required=False, widget=forms.Textarea)
	semester = forms.ChoiceField(label='Semester', choices=PILIHAN_SEMESTER)
	tahun = forms.ChoiceField(label='Tahun', choices=PILIHAN_TAHUN)
	ruang_kelas = forms.CharField(label='Ruang Kelas', max_length=50)